# README #

1/3スケールのSHARP X-1用3インチFDDユニット風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。 

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。 

***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1984年

## 参考資料

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-300f/raw/c285a8573e66c7a238603fbe34b5e67383843070/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-300f/raw/c285a8573e66c7a238603fbe34b5e67383843070/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-300f/raw/c285a8573e66c7a238603fbe34b5e67383843070/ExampleImage.png)
